module('first fixture', {
	setup: function() {
		// setup for module name 
	},
	teardown: function() {
		//teardown for module name
	}
});

test("some test that will succeeed", function(assert) {
	expect(1);
	assert.ok(true);
});

module('second fixture', {
  setup: function() {
    // setup for second fixture 
  },
  teardown: function() {
    //teardown for second fixture
  }
});

test("All numbers are floating point numbers", function(assert) {
	strictEqual(typeof 12, "number");
	strictEqual(typeof 12.34, "number");
});
