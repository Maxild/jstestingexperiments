/*jslint indent: 2, plusplus: false, forin: true, onevar: false, eqeqeq: false*/
/*globals assert, output*/

// TODO: Remove assert from global namespace/object
var assert = (function() {
  "use strict";

  function assert(message, expr) {
    if (!expr) {
      throw new Error(message);
    }

    assert.count++;

    return true;
  }

  assert.count = 0;

  return assert;
})();


var testCase = (function() {
  "use strict";
  
  function output(text, color) {
    var p = document.createElement("p");
    p.innerHTML = text;
    p.style.color = color;
    document.body.appendChild(p);
  }

  function testCase(name, tests) {

    assert.count = 0;

    var successful = 0;
    var testCount = 0;
    var hasSetup = typeof tests.setUp == "function";
    var hasTeardown = typeof tests.tearDown == "function";

    for (var test in tests) {

      if (!tests.hasOwnProperty(test) || !/^test/.test(test)) {
        continue;
      }

      testCount++;

      try {

        var fixture = {};

        if (hasSetup) {
          tests.setUp.call(fixture);
        }

        tests[test].call(fixture);

        output(test, "#0c0");

        if (hasTeardown) {
          tests.tearDown.call(fixture);
        }

        // If the tearDown method throws an error, it is
        // considered a test failure, so we don't count
        // success until all methods have run successfully
        successful++;
      } catch (e) {
        output(test + " failed: " + e.message, "#c00");
      }
    }

    var color = successful == testCount ? "#0c0" : "#c00";

    output("<strong>" + testCount + " tests, " +
      (testCount - successful) + " failures</strong>",
      color);
  }

  return testCase

})();